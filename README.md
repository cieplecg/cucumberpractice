# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

some simple BDD framework to use with selenium & cucumber

### How do I get set up? ###

1. move UserData.xlsx to /home/framework/test-input directory
2. run mvn clean test site
3. run mvn -Djetty.port=8888 jetty:run
4. go to localhost:8888

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

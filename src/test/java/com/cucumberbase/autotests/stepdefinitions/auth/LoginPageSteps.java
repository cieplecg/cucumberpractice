package com.cucumberbase.autotests.stepdefinitions.auth;

import com.cucumberbase.autotests.pageobjectsfactory.pageobject.homepage.HomePage;
import com.cucumberbase.autotests.pageobjectsfactory.pageobject.login.LoginPage;
import com.cucumberbase.utilities.logger.Log;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class LoginPageSteps {

    HomePage homePage = new HomePage();
    LoginPage loginPage = new LoginPage();

    @When("^Log in as \"(.*)\"$")
    public void logIlnAs(String actor){
        loginPage.logIlnAs(actor);
    }

    @When("^Enter valid email and submit form$")
    public void registerAs(){
        loginPage.registerAsNewUser();
    }

    @When("^User log out$")
    public void userLogOut(){
        homePage.logOut();
    }
}

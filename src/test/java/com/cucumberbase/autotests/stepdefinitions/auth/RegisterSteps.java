package com.cucumberbase.autotests.stepdefinitions.auth;

import com.cucumberbase.autotests.pageobjectsfactory.pageobject.login.components.RegisterComponentObject;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class RegisterSteps {

    RegisterComponentObject registerComponentObject = new RegisterComponentObject();

    @Then("^Registration error should exist with status (true|false)$")
    public void isErrorOccured(boolean blnShouldErrorExist){
        Assert.assertEquals(registerComponentObject.isErrorOccured(),blnShouldErrorExist, "Error display do not match as expected");
    }

    @When("^Submit registration form$")
    public void submitRegisterForm(){
        registerComponentObject.submitRegisterForm();
    }

    @Then("Registration page is displayed")
    public void registrationPageIsDisplayed(){
        Assert.assertTrue(registerComponentObject.isRegisterComponentDisplayed());
    }

    @When("^Fill in registration form$")
    public void fillInRegistrationForm(){
        registerComponentObject.fillInRegistrationForm();
    }
}

package com.cucumberbase.autotests.stepdefinitions.home;

import com.cucumberbase.autotests.pageobjectsfactory.pageobject.homepage.HomePage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class HomePageSteps {

    HomePage homePage = new HomePage();

    @When("^Open the home page$")
    public void verifyHomePageVisibility(){
        Assert.assertTrue(homePage.isAt());
    }

    @Then("I will be logged out")
    public void verifyUserIsLoggedOut(){
        Assert.assertTrue(homePage.isLoggedOut());
    }

    @Then("I will be logged in")
    public void isLoggedIn() {Assert.assertTrue(homePage.isLoggedIn());}

    @When("^Navigate to login page$")
    public void navigateToLoginPage(){
        homePage.openLoginPage();
    }
}

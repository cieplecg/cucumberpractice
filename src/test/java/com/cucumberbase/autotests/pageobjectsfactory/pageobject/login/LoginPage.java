package com.cucumberbase.autotests.pageobjectsfactory.pageobject.login;

import com.cucumberbase.autotests.pageobjectsfactory.BasePageObject;
import com.cucumberbase.autotests.pageobjectsfactory.pageobject.homepage.HomePage;
import com.cucumberbase.autotests.pageobjectsfactory.pageobject.login.components.RegisterComponentObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePageObject {

    @FindBy(id = "email")
    WebElement emailInput;
    @FindBy(id = "email_create")
    WebElement newEmailInput;
    @FindBy(id = "passwd")
    WebElement passwordInput;
    @FindBy(id = "SubmitLogin")
    WebElement submitLogin;
    @FindBy(id = "SubmitCreate")
    WebElement submitCreate;

    public final String EMAIL = getRandomEmail();
    private void setLoginEmail(String email){
        emailInput.sendKeys(email);
    }
    private void setLoginPassword(String password){
        passwordInput.sendKeys(password);
    }
    private void setRegisterEmail(String email){
        newEmailInput.sendKeys(email);
    }

    public HomePage logIlnAs(String actor){
        setLoginEmail(dataHelper.getActor(actor).get("login"));
        setLoginPassword(dataHelper.getActor(actor).get("password"));
        submitLogin.click();
        return new HomePage();
    }

    public RegisterComponentObject registerAsNewUser(){
        setRegisterEmail(EMAIL);
        submitCreate.click();
        return new RegisterComponentObject();
    }



}

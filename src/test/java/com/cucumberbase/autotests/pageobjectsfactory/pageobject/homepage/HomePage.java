package com.cucumberbase.autotests.pageobjectsfactory.pageobject.homepage;


import com.cucumberbase.autotests.pageobjectsfactory.BasePageObject;
import com.cucumberbase.autotests.pageobjectsfactory.pageobject.login.LoginPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePageObject {

    @FindBy(css = ".sf-menu")
    WebElement navigationBar;

    @FindBy(css = ".login")
    WebElement signInButton;

    @FindBy(css = ".logout")
    WebElement logoutButton;

    public boolean isAt(){
        return isElementOnPage(navigationBar);
    }
    public boolean isLoggedOut(){
        return isElementOnPage(signInButton);
    }
    public boolean isLoggedIn() {
        return isElementOnPage(logoutButton);
     }
    public LoginPage openLoginPage(){
          waitAndClick(signInButton);
          return new LoginPage();
    }
    public void logOut(){
        logoutButton.click();
    }
}

package com.cucumberbase.autotests.pageobjectsfactory.pageobject.login.components;

import com.cucumberbase.autotests.pageobjectsfactory.BasePageObject;
import com.cucumberbase.utilities.logger.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import static com.cucumberbase.autotests.common.State.CALIFORNIA;

public class RegisterComponentObject extends BasePageObject {

    @FindBy(id = "uniform-id_gender2")
    private WebElement mrsRadioButton;
    @FindBy(id = "uniform-id_gender1")
    private WebElement mrRadioButton;
    @FindBy(id = "customer_firstname")
    private WebElement firstNameCustomerInput;
    @FindBy(id = "customer_lastname")
    private WebElement lastNameCustomerInput;
    @FindBy(id = "email")
    private WebElement emailCustomerInput;
    @FindBy(id = "passwd")
    private WebElement passwordCustomerInput;
    @FindBy(id = "uniform-days")
    private WebElement daysDropdown;
    @FindBy(id = "uniform-months")
    private WebElement monthDropdown;
    @FindBy(id = "uniform-years")
    private WebElement yearsDropdown;
    @FindBy(id = "newsletter")
    private WebElement newsletterCheckbox;
    @FindBy(id = "uniform-optin")
    private WebElement optinCheckbox;
    @FindBy(id = "firstname")
    private WebElement firstNameAddressInput;
    @FindBy(id = "lastname")
    private WebElement lasttNameAddressInput;
    @FindBy(id = "company")
    private WebElement companyAddressInput;
    @FindBy(id = "address1")
    private WebElement address1CustomerInput;
    @FindBy(id = "address2")
    private WebElement address2CustomerInput;
    @FindBy(id = "city")
    private WebElement cityCustomerInput;
    @FindBy(id = "uniform-id_state")
    private WebElement stateCustomerInput;
    @FindBy(id = "postcode")
    private WebElement postcodeInput;
    @FindBy(id = "uniform-id_country")
    private Select countryCustomerInput;
    @FindBy(id = "other")
    private WebElement otherCustomerInput;
    @FindBy(id = "phone")
    private WebElement HomePhoneCustomerInput;
    @FindBy(id = "phone_mobile")
    private WebElement MobilePhoneCustomerInput;
    @FindBy(id = "alias")
    private WebElement aliasCustomerInput;
    @FindBy(id = "submitAccount")
    private WebElement submitAccountButton;
    public final String ERROR_CLASS = ".alert.alert-danger";
    public final String FIRST_NAME = getRandomString(8);
    public final String LAST_NAME = getRandomString(8);
    public final String PASSWORD = getRandomString(8);
    public final String COMPANY = getRandomString(8);
    public final String ADDRESS = getRandomString(8);
    public final String CITY = getRandomString(8);
    public final String POSTALCODE = getRandomDigits(5);
    public final String OTHER = getRandomString(8);
    public final String HOME_PHONE = getRandomDigits(9);
    public final String MOBILE_PHONE = getRandomDigits(9);
    public final String ADDRESS_REFERENCE = getRandomString(8);
    public final String TITLE = "Mr";

    public RegisterComponentObject setCustomerTitle(String title) {
        if (title.toLowerCase().equals("mrs")) {
            mrsRadioButton.click();
        } else if (title.toLowerCase().equals("mr")) {
            mrRadioButton.click();
        } else {
            //Nothing to do
        }
        return this;
    }
    public RegisterComponentObject setCustomerFirstName(String firstname) {
        firstNameCustomerInput.sendKeys(firstname);
        return this;
    }
    public RegisterComponentObject setCustomerLastName(String lastname) {
        lastNameCustomerInput.sendKeys(lastname);
        return this;
    }
    public RegisterComponentObject setCustomerEmail(String email) {
        emailCustomerInput.sendKeys(email);
        return this;
    }
    public RegisterComponentObject setCustomerPassword(String password) {
        passwordCustomerInput.sendKeys(password);
        return this;
    }
    public RegisterComponentObject setBirthDate(int day, int month, int year) {
        new Select(daysDropdown.findElement(By.id("days"))).selectByValue(String.valueOf(day));
        new Select(monthDropdown.findElement(By.id("months"))).selectByValue(String.valueOf(month));
        new Select(yearsDropdown.findElement(By.id("years"))).selectByValue(String.valueOf(year));
        return this;
    }
    public RegisterComponentObject setSubscriptionAlert(boolean toSet) {
        boolean isSelected = newsletterCheckbox.isSelected();
        if (!isSelected && toSet) {
            newsletterCheckbox.click();
        } else if (isSelected && !toSet) {
            newsletterCheckbox.click();
        }
        return this;
    }
    public RegisterComponentObject setOptinCheckbox(boolean toSet) {
        boolean isSelected = optinCheckbox.isSelected();
        if (!isSelected && toSet) {
            optinCheckbox.click();
        } else if (isSelected && !toSet) {
            optinCheckbox.click();
        }
        return this;
    }
    public RegisterComponentObject setCompanyAddressInput(String company) {
        companyAddressInput.sendKeys(company);
        return this;
    }
    public RegisterComponentObject setAddress1CustomerInput(String address) {
        address1CustomerInput.sendKeys(address);
        return this;
    }
    public RegisterComponentObject setAddress2CustomerInput(String address) {
        address2CustomerInput.sendKeys(address);
        return this;
    }
    public RegisterComponentObject setCityCustomerInput(String city) {
        cityCustomerInput.sendKeys(city);
        return this;
    }
    public RegisterComponentObject setStateCustomerInput(int state) {
        new Select(stateCustomerInput.findElement(By.id("id_state"))).selectByValue(String.valueOf(state));
        return this;
    }
    public RegisterComponentObject setCountryCustomerInput(String country) {
        countryCustomerInput.selectByValue(country);
        return this;
    }
    public RegisterComponentObject setOtherCustomerInput(String other) {
        otherCustomerInput.sendKeys(other);
        return this;
    }
    public RegisterComponentObject setHomePhoneCustomerInput(String phone) {
        HomePhoneCustomerInput.sendKeys(phone);
        return this;
    }
    public RegisterComponentObject setPostalCustomerInput(String postalCode){
        postcodeInput.sendKeys(postalCode);
        return this;
    }
    public RegisterComponentObject setMobilePhoneCustomerInput(String phone) {
        MobilePhoneCustomerInput.sendKeys(phone);
        return this;
    }
    public RegisterComponentObject setAliasCustomerInput(String alias) {
        aliasCustomerInput.sendKeys(alias);
        return this;
    }
    public boolean isErrorOccured(){
        waitForPageToLoad();
        boolean isErrorDisplayed = false;
        String message = "";
        try {
            isErrorDisplayed = driver.findElement(By.cssSelector(ERROR_CLASS)).isDisplayed();
        } catch (Exception e) {
            Log.error(e.toString());
        }
        return isErrorDisplayed;
    }
    public void submitRegisterForm(){
        submitAccountButton.click();
    }
    public boolean isRegisterComponentDisplayed(){
        wait.forElementClickable(mrsRadioButton);
        return mrsRadioButton.isDisplayed();
    }
    public void fillInRegistrationForm(){
        setCustomerTitle(TITLE);
        setCustomerFirstName(FIRST_NAME);
        setCustomerLastName(LAST_NAME);
        setCustomerPassword(PASSWORD);
        setBirthDate(13,11,1994);
        setSubscriptionAlert(true);
        setOptinCheckbox(true);
        setCompanyAddressInput(COMPANY);
        setAddress1CustomerInput(ADDRESS);
        setAddress2CustomerInput(ADDRESS);
        setCityCustomerInput(CITY);
        setStateCustomerInput(CALIFORNIA.id);
        setOtherCustomerInput(OTHER);
        setHomePhoneCustomerInput(HOME_PHONE);
        setPostalCustomerInput(POSTALCODE);
        setMobilePhoneCustomerInput(MOBILE_PHONE);
    }

}

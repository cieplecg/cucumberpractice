package com.cucumberbase.autotests.common.hooks;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import static com.cucumberbase.autotests.common.Constants.ENV_URL;
import static com.cucumberbase.utilities.driverconfig.DriverHandler.clearDriverCookies;
import static com.cucumberbase.utilities.driverconfig.DriverHandler.getDriver;

public class Hooks {

    //Before each feature navigate to environment url
    @Before
    public void beforeCucumberScenario(){
        getDriver().navigate().to(ENV_URL);
    }

    //After each feature clear cookies
    @After
    public void afterCucumberScenario(){
        clearDriverCookies();
    }

}

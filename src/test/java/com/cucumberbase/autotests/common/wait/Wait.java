package com.cucumberbase.autotests.common.wait;

import com.cucumberbase.utilities.logger.Log;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Wait {

    private static final int DEFAULT_TIMEOUT = 15;
    private static final String ELEMENT_PRESENT_MESSAGE = "ELEMENT PRESENT";
    private static final String ELEMENT_PRESENT_ERROR_FORMAT = "PROBLEM WITH FINDING ELEMENT %s";

    private WebDriver driver;
    private WebDriverWait wait;

    public Wait(WebDriver webDriver) {

        this.driver = webDriver;
        this.wait = new WebDriverWait(driver,DEFAULT_TIMEOUT);
    }

    public WebElement forElementPresent(By by) {
        return forElementPresent(by, DEFAULT_TIMEOUT);
    }

    public WebElement forElementPresent(By by, int timeout) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver, timeout).until(ExpectedConditions
                    .presenceOfElementLocated(by));
        } catch (TimeoutException e) {
            Log.error(ELEMENT_PRESENT_MESSAGE + String.format(ELEMENT_PRESENT_ERROR_FORMAT, by.toString()) + e.toString());
            throw e;
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public boolean forElementNotVisible(By by, int timeout) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver, timeout).until(
                    ExpectedConditions.invisibilityOfElementLocated(by));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public boolean forElementNotVisible(By by) {
        return forElementNotVisible(by, DEFAULT_TIMEOUT);
    }

    public WebElement forElementClickable(By by, int timeout) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver, timeout).until(ExpectedConditions
                    .elementToBeClickable(by));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public WebElement forElementClickable(WebElement element) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions
                    .elementToBeClickable(element));
        } catch (TimeoutException e) {
            Log.error(ELEMENT_PRESENT_MESSAGE + String.format(ELEMENT_PRESENT_ERROR_FORMAT, element.toString()) + e.toString());
            throw e;
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public WebElement forElementVisible(By by) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public WebElement forElementPresent(By by, boolean failOnTimeout) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return wait.until(ExpectedConditions.presenceOfElementLocated(by));
        } catch (TimeoutException e) {
            if (failOnTimeout) {
                Log.error(ELEMENT_PRESENT_MESSAGE +
                        String.format(ELEMENT_PRESENT_ERROR_FORMAT, by.toString()));
            }

            throw e;
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public void waitForPageLoad() {
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver wdriver){
                return ((JavascriptExecutor) driver).executeScript(
                        "return document.readyState"
                ).equals("complete");
            }
        });
    }

    public WebElement forElementVisible(WebElement element) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions
                    .visibilityOf(element));
        } catch (TimeoutException e) {
            Log.error(ELEMENT_PRESENT_MESSAGE + String.format(ELEMENT_PRESENT_ERROR_FORMAT, element.toString()) + e.toString());
            throw e;
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public WebElement forElementClickable(By by) {
        return forElementClickable(by, DEFAULT_TIMEOUT);
    }

    public void changeImplicitWait(int value, TimeUnit timeUnit) {
        driver.manage().timeouts().implicitlyWait(value, timeUnit);
    }

    public void restoreDefaultImplicitWait() {
        changeImplicitWait(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
    }


}

package com.cucumberbase.autotests.common.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import static com.cucumberbase.utilities.driverconfig.DriverHandler.*;

@CucumberOptions(
        features = {"src/test/resources/features"},
        glue = {"com.cucumberbase.autotests.stepdefinitions", "com.cucumberbase.autotests.common.hooks"}
)
public class TestRunner extends AbstractTestNGCucumberTests{

    //Initialize the environment
    @BeforeClass
    public void setup(){
        //initialize the driver factory
        instantiateDriverObject();
    }

    //Tear down the driver factory
    @AfterClass
    public void teardown(){
        closeDriverObjects();
    }

}

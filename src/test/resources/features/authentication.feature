@authorisation
Feature: Authentication
  User should be able to register

    @register
    Scenario: User register
      When Open the home page
        And Navigate to login page
        And Enter valid email and submit form
        And Fill in registration form
        And Submit registration form
      Then Registration error should exist with status false
       And I will be logged in
       And User log out



package com.cucumberbase.utilities.reporter;

import com.cucumberbase.utilities.logger.Log;
import gherkin.formatter.model.Result;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.cucumberjvm.AllureReporter;

import static com.cucumberbase.utilities.driverconfig.DriverHandler.getDriver;

public class AllureReporterExtended extends AllureReporter {

    @Override
    public void result(Result result) {
        if("failed".equals(result.getStatus())){
            attachScreenshot(getDriver());
        }
        super.result(result);
    }

    @Attachment(type = "image/png")
    private byte[] attachScreenshot(WebDriver driver){
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }
}

package com.cucumberbase.utilities.datahelper;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.cucumberbase.utilities.Constants.TEST_INPUT;

public class DataHelper {

	private List<HashMap<String,String>> myData = new ArrayList<>();

	//public constructor to initialize the array list
	public DataHelper(){
		try
		{

			FileInputStream fs = new FileInputStream(TEST_INPUT);
			XSSFWorkbook workbook = new XSSFWorkbook(fs);
			XSSFSheet sheet = workbook.getSheet("data");
			Row HeaderRow = sheet.getRow(0);

			for(int i=1;i<sheet.getPhysicalNumberOfRows();i++)
			{
				Row currentRow = sheet.getRow(i);
				HashMap<String,String> currentHash = new HashMap<String,String>();
				for(int j=0;j<currentRow.getPhysicalNumberOfCells();j++)
				{
					Cell currentCell = currentRow.getCell(j);
					currentHash.put(HeaderRow.getCell(j).getStringCellValue(), currentCell.getStringCellValue());
				}
				myData.add(currentHash);
			}

			fs.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	//public function to get the first element by name
	public HashMap<String,String> getActor(String actor){
		for (HashMap<String,String> data : myData) {
			if (data.get("userName").equals(actor)){
				return data;
			}
		}
		return null;
	}

}

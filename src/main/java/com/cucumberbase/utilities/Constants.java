package com.cucumberbase.utilities;

import static com.cucumberbase.utilities.propertieshandler.PathHandler.getPath;

public class Constants {
    public static final String TEST_INPUT = getPath("input.directory");
}

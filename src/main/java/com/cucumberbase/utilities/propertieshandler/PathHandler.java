package com.cucumberbase.utilities.propertieshandler;

import com.cucumberbase.utilities.logger.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PathHandler {

    private static Properties _properties;

    private static void init() {

        if (_properties == null) {
            _properties = new Properties();
            InputStream configurationFileIS = PathHandler.class.getClassLoader().getResourceAsStream("path.properties");
            try {
                _properties.load(configurationFileIS);
                Log.info("path.properties file was loaded.");
            } catch (IOException e) {
                Log.error("Exception: " + e.getMessage());
            }
        }
    }

    public static String getPath(String propertyName) {

        init();
        String homePath = System.getProperty("user.home");
        return homePath + _properties.getProperty(propertyName);
    }

    public static String getProperty(String propertyName) {

        init();
        return _properties.getProperty(propertyName);
    }
}

package com.cucumberbase.utilities.logger;

import org.apache.log4j.Logger;

public class Log {

    private static Logger Log = Logger.getLogger(Log.class.getName());

    public synchronized static void startTestCase(String sTestCaseName){

        Log.info("************************************************************************************************************");

        Log.info("----------------------       " + sTestCaseName + "       ----------------------");

        Log.info("************************************************************************************************************");

    }

    public synchronized static void endTestCase() {

        Log.info("*****************************               " + "-E---N---D-" + "               *****************************");
    }

    public synchronized static void info(String message) {

        Log.info(message);
    }

    public synchronized static void warn(String message) {

        Log.warn(message);
    }

    public synchronized static void error(String message) {

        Log.error(message);
    }

    public synchronized static void fatal(String message) {

        Log.fatal(message);
    }

    public synchronized static void debug(String message) {

        Log.debug(message);
    }

}
